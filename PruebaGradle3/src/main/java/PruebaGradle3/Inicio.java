package PruebaGradle3;

import java.util.ArrayList;

import NetworkTools.JavaMail;
import NetworkTools.Ping;
import Objetos.Monitoreador;
import Objetos.NodoDeRed;

public class Inicio {

	public static void main(String[] args) {
		ArrayList<NodoDeRed> nodos;
		nodos=ObtenerListaDeNodos();
        Monitoreador monitor=new Monitoreador();
        monitor.setIp("127.0.0.1");       
       // monitor.Monitorear(nodos);
        monitor.MonitorearHilos(nodos);
        
      //  System.out.println("Nodo: "+nodos.get(0).getNombre()+" status:"+nodos.get(0).estaOnline());
        
             
		
		
	}
	
	
	

	private static ArrayList<NodoDeRed> ObtenerListaDeNodos() {
		ArrayList<NodoDeRed> nodos=new ArrayList<NodoDeRed>();
		
		NodoDeRed nodo1=new NodoDeRed();
		NodoDeRed nodo2=new NodoDeRed();
		NodoDeRed nodo3=new NodoDeRed();
		
		nodo1.setNombre("127.0.0.1");
		nodo2.setNombre("8.8.8.8");
		nodo3.setNombre("127.0.0.3");
		
		nodo1.setIp("127.0.0.1");
		nodo2.setIp("8.8.8.8");
		nodo3.setIp("127.0.0.3");
		
		nodos.add(nodo1);
		nodos.add(nodo2);
		nodos.add(nodo3);
		
		
		return nodos;
	}

}
