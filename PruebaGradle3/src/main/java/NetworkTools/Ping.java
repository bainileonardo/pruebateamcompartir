package NetworkTools;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import Objetos.NodoDeRed;

public class Ping extends Thread{//inicio Ping
	private NodoDeRed nodo;
	
	

	public Ping(NodoDeRed nodo) {
		this.nodo=nodo;
		
	}
	
	public void run()
	   {
		brindarPorcentajePerdido();
		
	   }
	
	
public String brindarPorcentajePerdido() {// inicio responde
	String porcentajePerdido="";	
	String pingCmd = "ping " + this.nodo.getIp();
    try {
    	Runtime r = Runtime.getRuntime();
        Process p = r.exec(pingCmd);

        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String inputLine="Vacio";
        while ((inputLine = in.readLine()) != null) {
        	
            if(inputLine.contains("perdidos)")){
            	porcentajePerdido=inputLine;
            	porcentajePerdido=porcentajePerdido.replace(")", "");
            	porcentajePerdido=porcentajePerdido.replace("(", "");
            	porcentajePerdido=porcentajePerdido.replace(",", "");
            	porcentajePerdido=porcentajePerdido.replace("%", "");
            	porcentajePerdido=porcentajePerdido.replace("perdidos", "");
            	porcentajePerdido=porcentajePerdido.replace(" ", "");
            }              	
               

        	}
        in.close();
    	}
        	catch(Exception e) {
        		System.out.println(e.getMessage());
        	}

    		System.out.println("Escribir en la base -->"+nodo.getNombre()+" "+porcentajePerdido);
    	
		  
			return porcentajePerdido;
	
          }//fin responde
	    
public NodoDeRed getNodo() {
	return nodo;
}

public void setNodo(NodoDeRed nodo) {
	this.nodo = nodo;
}
	
	
	} // Fin Ping
	
	