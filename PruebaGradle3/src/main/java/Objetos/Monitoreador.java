package Objetos;

import java.util.ArrayList;
import java.util.Iterator;

import NetworkTools.Ping;


public class Monitoreador extends NodoDeRed{
	
	
	ArrayList<NodoDeRed> listaDeNodos;

	public Monitoreador() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	public void Monitorear(ArrayList<NodoDeRed> listaDeNodos) {
		Iterator<NodoDeRed> it=listaDeNodos.iterator();
		NodoDeRed nodo;
		Ping ping;
		while (it.hasNext()) {
			nodo=it.next();			
			ping=new Ping(nodo);
			if(ping.brindarPorcentajePerdido().equals("0")) {
				nodo.estaOnline=true;
				
				
			}else {
				nodo.estaOnline=false;
				
			}
			guardarEstadoEnLaBase(nodo);
		}
		
		
	}
	
	public void MonitorearHilos(ArrayList<NodoDeRed> listaDeNodos) {
		Iterator<NodoDeRed> it=listaDeNodos.iterator();
		NodoDeRed nodo;
		Ping ping;
		while (it.hasNext()) {
			nodo=it.next();			
			ping=new Ping(nodo);
			System.out.println("Monitoreando "+nodo.getIp());
			nodo=ping.getNodo();
			ping.start();			
			
		}
		
		
		
	}



	private void guardarEstadoEnLaBase(NodoDeRed nodo) {
		System.out.println("Guardando estado de "+nodo.getNombre());
		
	}
	
	

}
