package Objetos;

public class NodoDeRed {
	
	String ip;
	String macAddress;
	String nombre;
	String latitud;
	String longitud;
	boolean estaOnline;
	String porcentajePerdido;
	NodoDeRed nodoDelQueDepende;
	
    public String getSetPorcentajePerdido() {
		return porcentajePerdido;
	}
	public void setSetPorcentajePerdido(String porcentajePerdido) {
		this.porcentajePerdido = porcentajePerdido;
	}
	public boolean isEstaOnline() {
		return estaOnline;
	}
	public String setPorcentajePerdido;
	
	public boolean estaOnline() {
		return estaOnline;
	}
	public void setEstaOnline(boolean estaOnline) {
		this.estaOnline = estaOnline;
	}
	public NodoDeRed() {
		
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public NodoDeRed getNodoDelQueDepende() {
		return nodoDelQueDepende;
	}
	public void setNodoDelQueDepende(NodoDeRed nodoDelQueDepende) {
		this.nodoDelQueDepende = nodoDelQueDepende;
	}	
	
	

}
